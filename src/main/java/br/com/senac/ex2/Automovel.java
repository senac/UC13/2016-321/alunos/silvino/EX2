/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.ex2;

/**
 *
 * @author Administrador
 */
public class Automovel {
  private double custoDeFabrica;
    private double custoConsumidor;
    private double percentualDistribuidor;
    private double imposto;

    public Automovel() {
    }

    public Automovel(double custoDeFabrica, double percentualDistribuidor, double imposto) {
        this.custoDeFabrica = custoDeFabrica;
        this.percentualDistribuidor = percentualDistribuidor;
        this.imposto = imposto;
    }
    

    public double getCustoDeFabrica() {
        return custoDeFabrica;
    }

    public void setCustoDeFabrica(double custoDeFabrica) {
        this.custoDeFabrica = custoDeFabrica;
    }

    public double custoConsumidor() {
        return custoConsumidor;
    }

    public void custoConsumidor(double custoConsumidor) {
        this.custoConsumidor = custoConsumidor;
    }

    public double getPercentualDistribuidor() {
        return percentualDistribuidor;
    }

    public void setPercentualDistribuidor(double percentualDistribuidor) {
        this.percentualDistribuidor = percentualDistribuidor;
    }

    public double getImposto() {
        return imposto;
    }

    public void setImposto(double imposto) {
        this.imposto = imposto;
    }
    
    
    
    
    
}
