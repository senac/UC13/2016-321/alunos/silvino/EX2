package CalculoConsumidorTeste;

/**
 *
 * @author Administrador
 */
import br.com.senac.ex2.Automovel;
import br.com.senac.ex2.Calculadora;
import br.com.senac.ex2.CalculoDoConsumidor;
import org.junit.Test;
import static org.junit.Assert.*;

public class CalculoConsumidorTeste extends CalculoDoConsumidor{
    
@Test
    public void deveCalcularOcustoDoCarroAoConsumidor(){
        Calculadora calculadora = new Calculadora();
        Automovel a = new Automovel(15.000, 0.28, 0.45);
        
        double resultado = calculadora.calcular(a);
        assertEquals(25.950, resultado, 0.001);
        
        
        
        
        
    }
    
}

